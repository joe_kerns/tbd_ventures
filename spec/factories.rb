FactoryGirl.define do
  factory :restaurant do
    sequence(:name)  { |n| "Restaurant #{n}" }
    sequence(:phone) { |n| "#{n}"}
    sequence(:street_address) { |n| "#{n} Orchard St"}
    sequence(:city) { |n| "City #{n}"}
    rating "3"     
    state "New York"
    category "Food"
  end

   factory :server do
    name     "Joe Kerns"
    gender   "Male"
  end

	factory :user do
	    sequence(:first_name)  { |n| "first#{n}" }
	    sequence(:last_name)  { |n| "last#{n}" }
	    sequence(:username)  { |n| "username#{n}" }
	    sequence(:email)  { |n| "username#{n}@example.com" }
	    password "foobarasd"
	    password_confirmation "foobarasd"
	end
end