require 'spec_helper'

describe "RestaurantPages" do
  subject { page }
  
  shared_examples_for "all pages" do
    it { should have_title(full_title(page_title)) }  
    it { should have_selector("div#footer", text:"TBD Ventures") }
    it { should have_content("Sign in") }
    it { should have_content("Join today") }
  end   

	describe "new page" do
	    before { visit new_restaurant_path }
      let(:page_title) {"Create new restaurant"}

	    it { should have_content('Enter a new restaurant') }
	    it { should have_content('Name') }
      it { should have_content('Phone') }
      it { should have_content('Street address') }
	    it { should have_content('City') }
      it { should have_content('State') }
      it { should have_content('Category') }
	    
      it_should_behave_like "all pages"
	  	# it "should have the right links for the modal" do 
	  	# 	visit root_path
	  	# 	click_link "Get invited"
	   #  	expect(page).to have_content('Get excited'/)
	   #  end 
	end	

  describe "restaurant creation" do

    before do
      visit new_restaurant_path 
      click_button "Create"
    end 

    let(:submit) { "Create" }

    describe "with invalid information" do
      it "should not create a restaurant" do
        expect { click_button submit }.not_to change(Restaurant, :count)
      end

      describe "after submission" do
        before { click_button submit }

        it { should have_content('Enter a new restaurant') }
      end 
    end

    describe "with valid information" do
      before do
        fill_in "Restaurant name",        with: "Cafe Katja"
        fill_in "Restaurant phone number",		with: "(646) 111-1111"
        fill_in "Restaurant address", with: "30 Orchard Street"
        fill_in "Restaurant city", 		with: "New York"
        fill_in "Restaurant state",		with: "NY"
        fill_in "Restaurant category", 	with: "Austrian" #Fix to be dropdown
        fill_in "Restaurant rating", 		with: "5"  #Fix to be dropdown
      end

      it "should create a restaurant" do
        expect { click_button submit }.to change(Restaurant, :count).by(1)
      end

      #describe "after saving the restaurant" do
       # before { click_button submit }
        #let(:restaurant) { restaurant.find_by(email: 'restaurant@example.com') }

       # it { should have_title("TBD Ventures") }
     # end
    end
  end

  describe "restaurant page" do
    let(:restaurant) { FactoryGirl.create(:restaurant) }
    let(:page_title) {"#{restaurant.name}"}
    before { visit restaurant_path(restaurant) }

    it { should have_content(restaurant.name) }
    it { should have_content(restaurant.category)}    
    it_should_behave_like "all pages"
  end

  describe "edit restaurant" do
      let(:restaurant) { FactoryGirl.create(:restaurant) }
      let(:page_title) {"Edit restaurant"}

      before { visit edit_restaurant_path(restaurant) }

      describe "page" do
        it { should have_content("Edit restaurant") }
  
        it_should_behave_like "all pages"
      end

      describe "with invalid information" do
        before do
          fill_in "Restaurant name",        with: ""
          click_button "Save changes"
        end

        it { should have_content('error') }
      end

      describe "with valid information" do
        let(:new_name)  { "New name" }
        let(:new_phone) { "52039028934" }
        before do
          fill_in "Name",       with: new_name
          fill_in "Phone",            with: new_phone
          click_button "Save changes"
        end

        it { should have_title(new_name) }
        it { should have_selector('div.alert.alert-success') }
        specify { restaurant.reload.name.should  == new_name }
        specify { restaurant.reload.phone.should == new_phone }
      end

      # describe "forbidden attributes" do 
      #  let(:params) do
      #     { restaurant: { admin: true, password: restaurant.password,
      #               password_confirmation: restaurant.password } }
      #   end
      #   before do
      #     sign_in restaurant, no_capybara: true
      #     patch restaurant_path(restaurant), params
      #   end
      #   specify { expect(restaurant.reload).not_to be_admin }
      # end
  end

  describe "index" do
    let(:restaurant) { FactoryGirl.create(:restaurant) }
    before(:each) do
      #sign_in restaurant
      visit restaurants_path
    end

    it { should have_content('All restaurants') }

    describe "pagination" do

      before(:all) { 10.times { FactoryGirl.create(:restaurant) } }
      after(:all)  { Restaurant.delete_all }

      it { should have_selector('table') }

      it "should list each restaurant" do
        Restaurant.paginate(page: 1).each do |restaurant|
          expect(page).to have_content(restaurant.name)
        end
      end
    end

    # describe "delete links" do

    #   it { should_not have_link('delete') }

    #     describe "as an admin restaurant" do
     #      let(:admin) { FactoryGirl.create(:admin) }
     #      before do
     #        sign_in admin
     #        visit restaurants_index_path
     #      end

     #      it { should have_link('delete', href: restaurants_show_path(restaurant.first)) }
     #      it "should be able to delete another restaurant" do
     #        expect do
     #          click_link('delete', match: :first)
     #        end.to change(restaurant, :count).by(-1)
     #      end
     #      it { should_not have_link('delete', href: restaurants_show_path(admin)) }
    #     end
    # end
  end 
end
