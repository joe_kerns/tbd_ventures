require 'spec_helper'

describe "UserPages" do
	subject { page }

	describe "signup page" do 
		before { visit new_user_path }

		it { should have_content('Sign up') }
		it { should have_title(full_title('Sign up')) }
	end 

	describe "profile page" do
  	let(:user) { FactoryGirl.create(:user) }
	  
    before { visit user_path(user) }

	  it { should have_content(user.username) }
	  it { should have_title(user.username) }
	end
	
	describe "signup" do

	    before { visit new_user_path }

	    let(:submit) { "Create my account" }

	    describe "with invalid information" do
	      it "should not create a user" do
	        expect { click_button submit }.not_to change(User, :count)
	      end

	      describe "after submission" do
	        before { click_button submit }

	        it { should have_title(full_title('Sign up')) }
	        it { should have_content('error') }
	      end 
	    end

	    describe "with valid information" do
		    before do
				within(:css, "#user-info-form") do 
			        fill_in "First name",         with: "First"
			        fill_in "Last name",         with: "Last"
			        fill_in "Username",         with: "username"
			        fill_in "Email",        with: "user@example.com"
			        fill_in "Password",     with: "foobar123"
			        fill_in "Confirm Password", with: "foobar123"
		    	end 
		    end

	      it "should create a user" do
	        expect { click_button submit }.to change(User, :count).by(1)
	      end

	      describe "after saving the user" do
	        before { click_button submit }
	        let(:user) { User.find_by(email: 'user@example.com') }

	        it { should have_title(user.username) }
	        it { should have_selector('div.alert.alert-success', text: 'Welcome') }
	        #it { should have_link('Sign out') }
	      end
	    end
	end

	describe "edit" do
	    let(:user) { FactoryGirl.create(:user) }
	    before { visit edit_user_path(user) }

	    describe "page" do
	      it { should have_content("Update your profile") }
	      it { should have_title("Edit user") }
	      #it { should have_link('change', href: 'http://gravatar.com/emails') }
	    end

	    describe "with invalid information" do
	      before { click_button "Save changes" }

	      it { should have_content('error') }
	    end

	    describe "with valid information" do
	      let(:new_username)  { "New Username" }
	      let(:new_email) { "new@example.com" }
	      before do
	      	within(:css, "#user-info-form") do 
	        	fill_in "Username",       with: new_username
	        	fill_in "Email",            with: new_email
	        	fill_in "Password",         with: user.password
	        	fill_in "Confirm Password", with: user.password
	        	click_button "Save changes"
	        end 
	      end

	      it { should have_title(new_username) }
	      it { should have_selector('div.alert.alert-success') }
	      #it { should have_link('Sign out', href: signout_path) }
	      specify { user.reload.username.should  == new_username }
	      specify { user.reload.email.should == new_email }
	    end

	    # describe "forbidden attributes" do 
	    #  let(:params) do
	    #     { user: { admin: true, password: user.password,
	    #               password_confirmation: user.password } }
	    #   end
	    #   before do
	    #     sign_in user, no_capybara: true
	    #     patch user_path(user), params
	    #   end
	    #   specify { expect(user.reload).not_to be_admin }
	    # end
  end

  describe "index" do
    let(:user) { FactoryGirl.create(:user) }
    before(:each) do
      #sign_in user
      visit users_path
    end

    it { should have_content('All users') }

    describe "pagination" do

      before(:all) { 10.times { FactoryGirl.create(:user) } }
      after(:all)  { User.delete_all }

      it { should have_selector('table') }

      it "should list each user" do
        User.paginate(page: 1).each do |user|
          expect(page).to have_content(user.username)
        end
      end
    end

    # describe "delete links" do

    #   it { should_not have_link('delete') }

    #   	describe "as an admin user" do
	   #      let(:admin) { FactoryGirl.create(:admin) }
	   #      before do
	   #        sign_in admin
	   #        visit users_index_path
	   #      end

	   #      it { should have_link('delete', href: users_show_path(User.first)) }
	   #      it "should be able to delete another user" do
	   #        expect do
	   #          click_link('delete', match: :first)
	   #        end.to change(User, :count).by(-1)
	   #      end
	   #      it { should_not have_link('delete', href: users_show_path(admin)) }
    #   	end
    # end
  end

 end