require 'spec_helper'

describe "Server pages" do

  subject { page }

  describe "edit" do
    let(:server) { FactoryGirl.create(:server) }
    before { visit edit_server_path(server) }

    describe "page" do
      it { should have_title("Edit server") }
      it { should have_button('Save changes') }
    end
  end 

  describe "add server" do
    let(:server) { FactoryGirl.create(:server) }
    before { visit new_server_path }


    let(:submit) { "Create new server" }

    describe "with invalid information" do
      it "should not create a server" do
        expect { click_button submit }.not_to change(Server, :count)
      end
    end

    describe "with valid information" do
      before do
        fill_in "Name",         with: "Example User"
        fill_in "Gender",        with: "Male"
      end

      it "should create a server" do
        expect { click_button submit }.to change(Server, :count).by(1)
      end
    end
  end
end