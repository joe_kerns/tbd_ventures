require 'spec_helper'

describe Restaurant do
  before { @restaurant = Restaurant.new(name: "Cafe Katja", phone: "(646) 111-1111", street_address: "30 Orchard Street", city: "New York", state: "NY", category: "Austrian", rating: "5")}
	
  subject { @restaurant }

  it { should respond_to(:name) }
  it { should respond_to(:phone) }
  it { should respond_to(:street_address) }
  it { should respond_to(:city) }
  it { should respond_to(:state) }
  it { should respond_to(:category) }
  it { should respond_to(:rating) }
  it { should be_valid }
  
  describe "when name is not present" do
    before { @restaurant.name = " " }
    it { should_not be_valid }
	end 

  describe "when street address is not present" do
    before { @restaurant.street_address = " " }
    it { should_not be_valid }
    end 

  describe "when city is not present" do
    before { @restaurant.city = " " }
    it { should_not be_valid }
    end   
  
  describe "when state is not present" do
    before { @restaurant.state = " " }
    it { should_not be_valid }
    end 

  describe "when name is too long" do
    before { @restaurant.name = "a" * 51 }
    it { should_not be_valid }  
	end 

  #TEST FOR THE RESTAURANT ALREADY EXISTING 
  # describe "when email address is already taken" do
  #   before do
  #     user_with_same_email = @user.dup
  #     user_with_same_email.email = @user.email.upcase
  #     user_with_same_email.save
  #   end
  #   it { should_not be_valid }
  # end

  # describe "email address with mixed case" do
  #   let(:mixed_case_email) { "Foo@ExAMPle.CoM" }

  #   it "should be saved as all lower-case" do
  #     @user.email = mixed_case_email
  #     @user.save
  #     expect(@user.reload.email).to eq mixed_case_email.downcase
  #   end
  # end
  
  
  
  

end
