require 'spec_helper'

describe Server do

	before { @server = Server.new(name: "Example server", gender: "Gender")}

	subject { @server }

	it { should respond_to(:name)}
	it { should respond_to(:gender)}

	it {should be_valid }

	describe "when name is not present" do
		before { @server.name = " " }
		it {should_not be_valid }
	end

end
