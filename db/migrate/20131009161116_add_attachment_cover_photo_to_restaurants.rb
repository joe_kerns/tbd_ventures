class AddAttachmentCoverPhotoToRestaurants < ActiveRecord::Migration
  def self.up
    change_table :restaurants do |t|
      t.attachment :cover_photo
    end
  end

  def self.down
    drop_attached_file :restaurants, :cover_photo
  end
end
