class CreateRestaurants < ActiveRecord::Migration
  def change
    create_table :restaurants do |t|
      t.string :name
      t.string :phone
      t.string :street_address
      t.string :city
      t.string :state
      t.string :website
      t.string :category
      t.integer :rating

      t.timestamps
    end
  end
end
