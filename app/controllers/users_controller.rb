class UsersController < ApplicationController
	def new
		@user = User.new
	end

	def create
		@user = User.new(user_params)
		if @user.save
			flash[:success] = "Welcome to TBD #{@user.username}!"
			render 'show'
		else 
			flash[:error] = "#{@user.first_name}, we could not sign you up." 
      		render 'new'
		end
	end 

  	def show
  		@user = User.find(params[:id])
 	end

	def index
		@users = User.paginate(page: params[:page], per_page: 10)
	end

	def edit 
		@user = User.find(params[:id])
	end 

	def update
		@user = User.find(params[:id])
		if @user.update_attributes(user_params)
			flash[:success] = "#{@user.username} updated!"
			render 'show'
		else 
			flash[:error] = "#{@user.first_name}, we could not update your profile." 
      		render 'edit'
      	end 
	end 

	private

    def user_params
      params.require(:user).permit(:first_name, :last_name, :username, :email, :password,
                                   :password_confirmation, :profile_picture)
    end
end