class LayoutsController < ApplicationController
  def home
  	@restaurants = Restaurant.paginate(page: params[:page], per_page: 5)
  end

  def entities
  end

  def waiters
  end
end
