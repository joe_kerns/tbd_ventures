class RestaurantsController < ApplicationController
  def new
  	@restaurant = Restaurant.new
  end

  def create
    @restaurant = Restaurant.new(restaurant_params) 
    if @restaurant.save
      flash[:success] = "#{@restaurant.name} created!"
      render 'show'
    else
	    flash[:error] = "#{@restaurant.name} could not be created." 
      render 'new'
    end
  end 

  def show
    @restaurant = Restaurant.find(params[:id])
  end

  def index 
    @restaurants = Restaurant.paginate(page: params[:page], per_page: 10)
  end

  def edit
    @restaurant = Restaurant.find(params[:id])
  end 

  def update
    @restaurant = Restaurant.find(params[:id])
    if @restaurant.update_attributes(restaurant_params)
      flash[:success] = "#{@restaurant.name} updated!"
      render 'show'
    else 
      flash[:error] = "#{@restaurant.name} could not be updated." 
      render 'edit' 
    end 
  end 

  private

    def restaurant_params
      params.require(:restaurant).permit(:name, :phone, :street_address, :city, :state, :website, :category, :rating)
    end 
end
