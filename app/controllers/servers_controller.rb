class ServersController < ApplicationController
  
	def show
		@server = Server.find(params[:id])
	end

	def index
		@servers = Server.paginate(page: params[:page])
	end

	def edit
		@server = Server.find(params[:id])
	end

	def update
		@server = Server.find(params[:id])
		if @server.update_attributes(server_params)
			flash[:success] = "Your server has been updated!"
			redirect_to @server
		else
			render "edit"
		end
	end



	def destroy
	end

	def create
		@server = Server.new(server_params)
		if @server.save
			flash[:success] = "Thanks for adding a server!"
			redirect_to @server
		else
			render "show"
		end
	end

  	def new
  		@server = Server.new
  	end

  	def server_params
      params.require(:server).permit(:name, :gender)
    end
end
