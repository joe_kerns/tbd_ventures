module ServersHelper

	# Returns the Gravatar (http://gravatar.com/) for the given user.
  def gravatar_for(server)
    gravatar_id = Digest::MD5::hexdigest(server.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}"
    image_tag(gravatar_url, alt: server.name, class: "gravatar")
  end
end
