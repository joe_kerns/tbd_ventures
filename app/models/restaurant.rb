class Restaurant < ActiveRecord::Base
	attr_accessible :name, :phone, :street_address, :city, :state, :category, :rating, :cover_photo
	validates :name, presence: true, length: { maximum: 50 }
	validates :phone, presence: true
	validates :street_address, presence: true
	validates :city, presence: true
	validates :state, presence: true
	validates :category, presence: true
	validates :rating, numericality: true
	has_attached_file :cover_photo, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png" 
	#VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
	#validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
	default_scope -> { order('rating DESC') }
end
