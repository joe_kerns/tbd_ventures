class User < ActiveRecord::Base
	attr_accessible :first_name, :last_name, :username, :email, :password, :password_confirmation, :profile_picture
	has_secure_password
	
	validates :first_name, presence: true, length: { maximum: 20 }
	validates :last_name, presence: true, length: { maximum: 20 }
	validates :username, presence: true, length: { maximum: 20 }, uniqueness: { case_sensitive: false}
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
	validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
	#VALID_PASSWORD_REGEX = /^(?=.*\d)(?=.*([a-z]|[A-Z]))([\x20-\x7E]){8,40}$/
	validates :password, presence: true, length: { minimum: 6 } #, format: { with: VALID_PASSWORD_REGEX, multiline: true }
	has_attached_file :profile_picture, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png" 
	#default_scope -> { order('created_at DESC') }
	
	before_save { |user| user.email = email.downcase }
  	#before_create :create_remember_token
	

	def User.new_remember_token
    	SecureRandom.urlsafe_base64
  	end

  	def User.encrypt(token)
    	Digest::SHA1.hexdigest(token.to_s)
  	end

	private
	def create_remember_token
		self.remember_token = SecureRandom.urlsafe_base64
	end 

end
