namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    make_users(15)
    make_restaurants(15)
    make_servers(15)
  end

  def make_users(number)
    number.times do |n|
      first_name  = Faker::Name.first_name
      last_name = Faker::Name.last_name
      username = Faker::Internet.user_name
      email = Faker::Internet.email
      password = "password"
      profile_picture = File.open(Dir.glob(File.join(Rails.root, 'profilepictures', '*')).sample)
      User.create!(first_name: first_name, last_name: last_name, username: username, email: email, password: password, password_confirmation: password, profile_picture: profile_picture)
    end
  end 

  def make_restaurants(number)
    number.times do |n|
      name  = Faker::Company.name
      phone = Faker::PhoneNumber.phone_number
      street_address = Faker::Address.street_address
      city = Faker::Address.city
      state = Faker::Address.state
      category = Faker::Lorem.word
      rating = Faker::Number.digit
      cover_photo = File.open(Dir.glob(File.join(Rails.root, 'sampleimages', '*')).sample)
      Restaurant.create!(name: name, phone: phone, street_address: street_address, city: city, state: state, category: category, rating: rating, cover_photo: cover_photo)
    end
  end 
  
  def make_servers(number)
    number.times do |n|
      name  = Faker::Name.name
      gender = "gender"
      Server.create!(name: name, gender: gender)
    end 
  end 
end